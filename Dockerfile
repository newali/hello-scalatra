FROM tomcat
RUN apt-get update && apt-get clean
copy target/scalatra-maven-prototype.war /usr/local/tomcat/webapps/scalatra-maven-prototype.war
EXPOSE 8080
CMD ["catalina.sh", "run"]

